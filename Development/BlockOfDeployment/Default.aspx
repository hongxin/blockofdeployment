﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="BlockOfDeployment.Default" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Block of Deployment</title>
    <style>
        .link {
            width:100%;
            margin:15px 0;
            display:block;
            text-decoration:none;
            background-color:#111;
            font-size:24px;
            text-align:center;
            padding:10px 0;
            color:#FFF;
            border-radius:10px;
            box-sizing:border-box;
            border:none;
            font-family:'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande', 'Lucida Sans', Arial, sans-serif;
            cursor:pointer;
        }
        input {
            width:100%;
            margin:5px 0;
            padding:10px 20px;
            box-sizing:border-box;
            text-align:right;
        }
        .fluid {
            width:100%;
        }
        .container {
            width:300px;
            margin:0 auto;
        }
        .col-6 {
            width:49%;display:inline-block;
        }
        .text-right {
            text-align:right;
        }
        h1 {
            font-size:64px;
            text-align:center;
            margin:0;
            padding:0;
        }
        ol {
            margin:0;
        }
    </style>
    <script src="/Scripts/jquery-3.3.1.min.js"></script>
    <script>
        function startCustomMap() {
            var col = document.getElementById("txtCol").value;
            var row = document.getElementById("txtRow").value;
            var url = "Game?col=" + col + "&row=" + row;
            var win = window.open(url, '_blank');
            win.focus();
        }
        function validate(obj) {
            if (!obj.checkValidity()) {
                var num = parseInt(obj.value);
                if (isNaN(num)) {
                    obj.value = obj.defaultValue;
                } else {
                    if (num > obj.max) {
                        obj.value = obj.max;
                    }
                    if (num < obj.min) {
                        obj.value = obj.min;
                    }
                }
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server" onsubmit="return false;">
        <div class="fluid">
                <h1>Block of Deployment</h1>
            <div class="container">
                <a href="/Game?map=small" class="link" target="_blank">Small Map</a>
                <a href="/Game" class="link" target="_blank">Medium Map</a>
                <a href="/Game?map=large" class="link" target="_blank">Large Map</a>
                <hr />
                <div class="col-6 text-right">Columns:</div>
                <div class="col-6">
                    <input type="number" value="40" id="txtCol" class="half" min="6" max="96" step="1" required="required" onblur="validate(this)" />
                </div>
                <div class="col-6 text-right">Rows:</div>
                <div class="col-6">
                    <input type="number" value="20" id="txtRow" class="half" min="6" max="48" step="1" required="required" onblur="validate(this)" />
                </div>
                <button class="link" onclick="startCustomMap()">Custom Map</button>
                <hr />
                Rules:
                <ol>
                    <li>Red player start first block from top left corder</li>
                    <li>Blue player start first block from bottom right corder</li>
                    <li>Your block must be connected to your existing blocks (adjacently)</li>
                    <li>If you cannot place a block, you must end turn</li>
                    <li>Player with the most blocks wins</li>
                </ol>

            </div>
        </div>
    </form>
</body>
</html>
