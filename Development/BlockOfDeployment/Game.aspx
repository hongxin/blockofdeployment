﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Game.aspx.cs" Inherits="BlockOfDeployment.Game" %>
<%
    var mode = "";
    var max_row = 28;
    var max_col = 56;
    var map = Request.QueryString["map"];
    var row = Request.QueryString["row"];
    var col = Request.QueryString["col"];
    if (!String.IsNullOrEmpty(map)) {
        if (map == "large") {
            mode = "large";
            max_row = 48;
            max_col = 96;
        }
        if (map == "small") {
            mode = "small";
            max_row = 20;
            max_col = 40;
        }
    } else {
        if (!String.IsNullOrEmpty(row)) {
            try {
                var num = Int32.Parse(row);
                if (num > 48) num = 48;
                if (num > 28) mode = "large";
                if (num < 6) num = 6;
                max_row = num;
            } catch { }
        }
        if (!String.IsNullOrEmpty(col)) {
            try {
                var num = Int32.Parse(col);
                if (num > 96) num = 96;
                if (num > 56) mode = "large";
                if (num < 6) num = 6;
                max_col = num;
            } catch { }
        }
        if (max_row <= 20 && max_col <= 40) {
            mode = "small";
        }
    }

%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Block of Deployment</title>
    <style>
        body {
            margin:0;
            padding:5px;
        }
        table {
            margin:5px auto;
            border-spacing: 0;
        }
        td {
            width:20px;
            height:20px;
            text-align:center;
            vertical-align:middle;
        }
        table.large td {
            width:10px;
            height:10px;
        }
        table.small td {
            width:30px;
            height:30px;
        }
        .ui {
            position:fixed;
        }
        #player1-ui {
            left:5px;
            text-align:left;
        }
        #player2-ui {
            right:5px;
            text-align:right;
        }
        .nickname {
            font-weight:bold;
            padding:5px;
            display:inline-block;
            border-radius:5px;
            background-color:#EEE;
        }
        #player1-ui .nickname {
            background-color:#F00;
            color:#FFF;
        }
        #player2-ui .nickname {
            background-color:#00F;
            color:#FFF;
        }
        #player1-btn, #player2-btn {
            padding:4px 8px;
            cursor:pointer;
        }
        #player1-btn:disabled, #player2-btn:disabled {
            color:#DDD;
            cursor:not-allowed;
        }
        .row {
            margin-bottom:10px;
        }
        .cell {
            cursor:pointer;
        }
        .cell.place {
            background-color:#FF0;
        }
        .cell.p1 {
            background-color:#F00;
        }
        .cell.place1 {
            background-color:#FAA;
        }
        .cell.p2 {
            background-color:#00F;
        }
        .cell.place2 {
            background-color:#AAF;
        }
        .cell.p1.place1,
        .cell.p1.place2,
        .cell.p2.place1,
        .cell.p2.place2
        {
            background-color:#000;
        }
        .btn {
        }
        .btn.selected {
            background-color:#CCC;
            border: none;
        }
    </style>
    <script type="text/javascript">
        var dice1a = 0, dice1b = 0, dice2a = 0, dice2b = 0;
        function RollDice1() {
            //if (blnRolledDice) return;
            if (player_turn != 1) return;
            blnRolledDice = true;
            if (dice1a == 0) dice1a = Math.floor((Math.random() * 6) + 1);
            if (dice1b == 0) dice1b = Math.floor((Math.random() * 6) + 1);
            var btnA = document.getElementById("player1-btnA");
            var btnB = document.getElementById("player1-btnB");
            btnA.innerHTML = dice1b + "x" + dice1a;
            btnB.innerHTML = dice1a + "x" + dice1b;
            btnA.style.width = 30 + dice1b * 5 + "px";
            btnA.style.height = 30 + dice1a * 5 + "px";
            btnB.style.width = 30 + dice1a * 5 + "px";
            btnB.style.height = 30 + dice1b * 5 + "px";
            btnA.style.display = "inline-block";
            btnB.style.display = "inline-block";
            if (dice1a == dice1b) {
                //hide
                btnB.style.display = "none";
            }
            RemoveClass(btnA, "selected");
            RemoveClass(btnB, "selected");
            AddClass(btnA, "selected");
        }
        function RollDice2() {
            //if (blnRolledDice) return;
            if (player_turn != 2) return;
            blnRolledDice = true;
            if (dice2a == 0) dice2a = Math.floor((Math.random() * 6) + 1);
            if (dice2b == 0) dice2b = Math.floor((Math.random() * 6) + 1);
            var btnA = document.getElementById("player2-btnA");
            var btnB = document.getElementById("player2-btnB");
            btnA.innerHTML = dice2b + "x" + dice2a;
            btnB.innerHTML = dice2a + "x" + dice2b;
            btnA.style.width = 30 + dice2b * 5 + "px";
            btnA.style.height = 30 + dice2a * 5 + "px";
            btnB.style.width = 30 + dice2a * 5 + "px";
            btnB.style.height = 30 + dice2b * 5 + "px";
            btnA.style.display = "inline-block";
            btnB.style.display = "inline-block";
            if (dice2a == dice2b) {
                //hide
                btnB.style.display = "none";
            }
            RemoveClass(btnA, "selected");
            RemoveClass(btnB, "selected");
            AddClass(btnA, "selected");
        }
        function AddClass(element, name) {
            var arr = element.className.split(" ");
            if (arr.indexOf(name) == -1) {
                element.classList.add(name);
            }
        }
        function RemoveClass(element, name) {
            element.classList.remove(name);
        }
        function MouseEnterCell(cell) {
            //$(cell).addClass("place");
            if (blnRolledDice) {
                if (!blnPlaced) {
                    if (player_turn == 1) {
                        if (blnInvert) {
                            HighlightCells(cell, dice1a, dice1b, "place1");
                        } else {
                            HighlightCells(cell, dice1b, dice1a, "place1");
                        }
                    }
                    if (player_turn == 2) {
                        if (blnInvert) {
                            HighlightCells(cell, dice2a, dice2b, "place2");
                        } else {
                            HighlightCells(cell, dice2b, dice2a, "place2");
                        }
                    }
                }
            }
        }
        function MouseLeaveCell(cell) {
            //$(cell).removeClass("place");
            //$(cell).removeClass("place1");
            //$(cell).removeClass("place2");
        }
        function MouseClickCell(cell) {
            if (blnPlaced) {
                //remove placement
            } else if (blnRolledDice) {
                var blnError = false;
                if (player_turn == 1) {
                    $(".cell.p1.place1").each(function () {
                        blnError = true;
                    });
                    $(".cell.p2.place1").each(function () {
                        blnError = true;
                    });
                }
                if (player_turn == 2) {
                    $(".cell.p1.place2").each(function () {
                        blnError = true;
                    });
                    $(".cell.p2.place2").each(function () {
                        blnError = true;
                    });
                }
                if (blnError) return;

                //TODO:restrict placement

                $(".cell.place1").each(function () {
                    $(this).removeClass("place1");
                    $(this).addClass("p1");
                });
                $(".cell.place2").each(function () {
                    $(this).removeClass("place2");
                    $(this).addClass("p2");
                });
                blnPlaced = true;
                EndTurn();
            }
        }
        function ChangeBlock1(x) {
            var btnA = document.getElementById("player1-btnA");
            var btnB = document.getElementById("player1-btnB");
            RemoveClass(btnA, "selected");
            RemoveClass(btnB, "selected");
            if (x == 2) {
                AddClass(btnB, "selected");
                blnInvert = true;
            } else {
                AddClass(btnA, "selected");
                blnInvert = false;
            }
            HightlightClear();
        }
        function ChangeBlock2(x) {
            var btnA = document.getElementById("player2-btnA");
            var btnB = document.getElementById("player2-btnB");
            RemoveClass(btnA, "selected");
            RemoveClass(btnB, "selected");
            if (x == 2) {
                AddClass(btnB, "selected");
                blnInvert = true;
            } else {
                AddClass(btnA, "selected");
                blnInvert = false;
            }
            HightlightClear();
        }
        //1 = player 1's turn
        //2 = player 2's turn
        //0 = init
        var player_turn = 0;
        var blnRolledDice = false;
        var blnPlaced = false;
        var blnInvert = false;
        function EndTurn() {
            if (player_turn == 1) {
                player_turn = 2;
                var btn1 = document.getElementById("player1-btn");
                var btn2 = document.getElementById("player2-btn");
                btn1.disabled = true;
                btn2.disabled = false;
                ShowButton2();
                HideButton1();
            } else {
                player_turn = 1;
                var btn1 = document.getElementById("player1-btn");
                var btn2 = document.getElementById("player2-btn");
                btn1.disabled = false;
                btn2.disabled = true;
                ShowButton1();
                HideButton2();
            }
            dice1a = 0;
            dice1b = 0;
            dice2a = 0;
            dice2b = 0;
            blnRolledDice = false;
            blnPlaced = false;
            blnInvert = false;
            var score1 = 0;
            var score2 = 0;
            $(".cell.p1").each(function () {
                score1++;
            });
            $(".cell.p2").each(function () {
                score2++;
            });
            document.getElementById("player1-score").innerHTML = score1;
            document.getElementById("player2-score").innerHTML = score2;
            var left = max_row * max_col - score1 - score2;
            document.getElementById("left-score").innerHTML = left + " block(s) left";
            document.getElementById("player1-score").style.fontWeight = "normal";
            document.getElementById("player2-score").style.fontWeight = "normal";
            if (player_turn == 2) {
                if (score1 > score2 + left) {
                    document.getElementById("player1-score").style.fontWeight = "bold";
                    PlayAudio();
                    alert("Player 1 won!");
                } else if (score2 > score1 + left) {
                    document.getElementById("player2-score").style.fontWeight = "bold";
                    PlayAudio();
                    alert("Player 2 won!");
                }
            } else {
                if (score2 > score1 + left) {
                    document.getElementById("player2-score").style.fontWeight = "bold";
                    PlayAudio();
                    alert("Player 2 won!");
                } else if (score1 > score2 + left) {
                    document.getElementById("player1-score").style.fontWeight = "bold";
                    PlayAudio();
                    alert("Player 1 won!");
                }
            }
            HightlightClear();
        }
        var blnWon = false;
        function PlayAudio() {
            if (!blnWon) {
                blnWon = true;
    		    document.getElementById('audio1').play();
            }
        }
        function ShowButton1() {
            var btnA = document.getElementById("player1-btnA");
            var btnB = document.getElementById("player1-btnB");
            var btnC = document.getElementById("player1-btnC");
            btnA.style.display = "none";
            btnB.style.display = "none";
            btnC.style.display = "inline-block";
        }
        function ShowButton2() {
            var btnA = document.getElementById("player2-btnA");
            var btnB = document.getElementById("player2-btnB");
            var btnC = document.getElementById("player2-btnC");
            btnA.style.display = "none";
            btnB.style.display = "none";
            btnC.style.display = "inline-block";
        }
        function HideButton1() {
            var btnA = document.getElementById("player1-btnA");
            var btnB = document.getElementById("player1-btnB");
            var btnC = document.getElementById("player1-btnC");
            btnA.style.display = "none";
            btnB.style.display = "none";
            btnC.style.display = "none";
        }
        function HideButton2() {
            var btnA = document.getElementById("player2-btnA");
            var btnB = document.getElementById("player2-btnB");
            var btnC = document.getElementById("player2-btnC");
            btnA.style.display = "none";
            btnB.style.display = "none";
            btnC.style.display = "none";
        }
        function HighlightCells(cell, width, height, highlight) {
            HightlightClear();
            var index = parseInt(cell.id.substring(5));
            //console.log("index: " + index);
            var x = (index % max_col);
            if (x > max_col - width) {
                index -= x - max_col + width;
            }
            var y = Math.floor(index / max_col);
            if (y > max_row - height) {
                index -= (y - max_row + height) * max_col;
            }
            for (var i = 0; i < height; i++) {
                for (var j = 0; j < width; j++) {
                    var id = index + i * max_col + j;
                    $("#cell_" + id).addClass(highlight);
                }
            }
        }
        function HightlightClear() {
            $(".cell").each(function () {
                $(this).removeClass("place");
                $(this).removeClass("place1");
                $(this).removeClass("place2");
            });
        }
    </script>
    <script src="/Scripts/jquery-3.3.1.min.js"></script>
</head>
<body>
    <div id="player1-ui" class="ui">
        <div class="row">
            <span class="nickname">Player 1</span>
        </div>
        <div class="row">
            <span id="player1-score">0</span> block(s)
        </div>
        <div class="row">
            <button id="player1-btn">Roll Dice</button>
        </div>
        <div class="row">
            <button id="player1-btnA" onclick="ChangeBlock1(1)" class="btn selected" style="display:none;"></button>
            <button id="player1-btnB" onclick="ChangeBlock1(2)" class="btn" style="display:none;"></button>
        </div>
        <div class="row">
            <button id="player1-btnC" onclick="EndTurn()">End Turn</button>
        </div>
    </div>
    <div id="player2-ui" class="ui">
        <div class="row">
            <span class="nickname">Player 2</span>
        </div>
        <div class="row">
            <span id="player2-score">0</span> block(s)
        </div>
        <div class="row">
            <button id="player2-btn">Roll Dice</button>
        </div>
        <div class="row">
            <button id="player2-btnA" onclick="ChangeBlock2(1)" class="btn selected" style="display:none;"></button>
            <button id="player2-btnB" onclick="ChangeBlock2(2)" class="btn" style="display:none;"></button>
        </div>
        <div class="row">
            <button id="player2-btnC" onclick="EndTurn()">End Turn</button>
        </div>
    </div>
    <div style="text-align:center;"><%=$"{max_col} × {max_row}" %></div>
    <table border="1" class="<%=mode%>">
        <tbody>
<%
    for(var i=0;i<max_row;i++) {
        Response.Write("<tr>");
        for (var j = 0; j < max_col; j++) {
            var index = i * max_col + j;
            Response.Write($"<td class='cell' id='cell_{index}' data-index='{index}'></td>");
        }
        Response.Write("</tr>");
    }
%>
        </tbody>
    </table>
    <div style="text-align:center;" id="left-score"></div>
    <script>
        var max_row = <%=max_row%>;
        var max_col = <%=max_col%>;
        var cells = new Array(max_row*max_col);
        jQuery(document).ready(function () {
            $(".cell").each(function () {
                $(this).mouseenter(function () {
                    MouseEnterCell(this);
                });
                $(this).mouseleave(function () {
                    MouseLeaveCell(this);
                });
                $(this).click(function () {
                    MouseClickCell(this);
                });
            });
            EndTurn();
            var btn1 = document.getElementById("player1-btn");
            btn1.onclick = RollDice1;
            var btn2 = document.getElementById("player2-btn");
            btn2.onclick = RollDice2;
            player_turn = 1;
            blnRolledDice = false;
            blnPlaced = false;
            window.onbeforeunload = function () {
                return "Are you sure?";
            };
        });
    </script>
    <audio id="audio1">
        <source src="winner.ogg" type="audio/ogg" />
        <source src="winner.mp3" type="audio/mpeg" />
    </audio>
</body>
</html>
