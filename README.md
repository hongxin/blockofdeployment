# BlockOfDeployment

A mini-game inspired by a post from 9GAG

Download the entire **"Demo"** folder, open **Game.html** or **QuickDemo.html** with web browser (recommend: Google Chrome)

Incomplete Tasks:
- restrict block placement on first turn (top left corner for first player and bottom right corner for second player)
- restrict block placement if it is not connected to existing blocks.

# Reference
9GAG post: https://www.facebook.com/9gag/photos/a.109041001839/10158339550946840/

Rules (original):
1. roll 2 dice. 
2. make a rectangle with the sides generated
3. The rectangle must be connected to your existing territory.
4. The first players first rectangle is placed in a corner. The second players first rectangle in the opposing corner.
5. If you cannot make the rectangle generated you skip your turn.
6. When all space is filled you end the game.
7. The one with the most territory wins.

